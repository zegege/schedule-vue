import axios from 'axios'

// // 设置axios的基础url
// axios.defaults.baseURL = 'http://127.0.0.1:8888/api'

// 所有请求带上Token
axios.interceptors.request.use(config => {
  // 请求对象添加Authorization
  config.headers.Authorization = 'JWT ' + window.sessionStorage.getItem('token')
  return config
})

// login页登录
export const login = params => {
  return axios.post('/api/login/', params)
}

// identify页
export const identify = () => {
  return axios.get('/api/user/identify/')
}

// 用户列表页
export const userList = (params) => {
  return axios.get('/api/user/users/', { params: params })
}

// 添加用户
export const userAdd = params => {
  return axios.post('/api/user/users/', params)
}

// 查询单个用户
export const userGet = id => {
  return axios.get(`/api/user/users/${id}/`)
}

// 修改用户
export const userUpdate = (id, params) => {
  // eslint-disable-next-line no-prototype-builtins
  if (params.hasOwnProperty('birthday')) {
    params.birthday = params.birthday.slice(0, 10)
  }

  return axios.put(`/api/user/users/${id}/`, params)
}
// 删除用户
export const userDel = id => {
  return axios.delete(`/api/user/users/${id}/`)
}

// 我的选课
export const myCourses = () => {
  return axios.get('/api/course/course/my_course/')
}

// 课程列表
export const coursesGet = () => {
  return axios.get('/api/course/course/')
}

// 查询单个课程
export const coursesRead = id => {
  return axios.get(`/api/course/course/${id}/`)
}
// 删除课程
export const coursesDel = id => {
  return axios.delete(`/api/course/course/${id}/`)
}

// 创建课程
export const CourseCreate = params => {
  return axios.post('/api/course/course/', params)
}

// 选课 与 取消课程
export const ChangeCourse = (params) => {
  return axios.post('/api/user/users/change_course/', params)
}

// 课程修改
export const UpdateCourse = (id, params) => {
  return axios.put(`/api/course/course/${id}/`, params)
}
