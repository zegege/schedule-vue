import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/home',
  name: 'Home',
  component: Home,
  children: [{
    path: '/users',
    name: 'Users',
    component: () => import('@/views/User.vue')
  }, {
    path: '/mycourse',
    name: 'MyCourse',
    component: () => import('@/views/MyCourse.vue')
  }, {
    path: '/course',
    name: 'Course',
    component: () => import('@/views/Course.vue')
  }
  ],
  redirect: {
    name: 'Course'
  }
}, {
  path: '/login',
  name: 'Login',
  component: () => import('@/views/Login.vue')
}, {
  // 默认 跳转
  path: '/',
  redirect: '/home'
}]

const router = new VueRouter({
  routes
})

// 路由守卫
router.beforeEach((to, from, next) => {
  // 判断是否有token
  if (to.name !== 'Login') {
    if (!window.sessionStorage.getItem('token')) return next('/login')
    // 判断有了token是否是请求的login页
  } else if (to.path === '/login' && window.sessionStorage.getItem('token')) return next(false)
  next()
})

export default router
